﻿using c_client.connection;
using c_client.LogIn;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_client
{
    public partial class LogInForm : Form
    {
        Client client;
        internal LogInForm(Client c)
        {
            InitializeComponent();
            client = c;
            this.PassBox.KeyPress += new KeyPressEventHandler(CheckEnter);
            this.UserBox.KeyPress += new KeyPressEventHandler(CheckEnter);
            this.ControlBox = false;
        }

        private void LogInForm_Load(object sender, EventArgs e)
        {

        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            logInMethod();
        }

        private void logInMethod()
        {
            if (UserBox.Text != "" && PassBox.Text != "")
            {
                if (client.logIn(new LogInRequest(UserBox.Text, PassBox.Text)))
                {
                    this.Close();

                }
                else
                {
                    Shake(this);
                    UserBox.Clear();
                    PassBox.Clear();
                }
            }
        }

        private static void Shake(Form form)
        {
            var original = form.Location;
            var rnd = new Random(1337);
            const int shake_amplitude = 20;
            for (int i = 0; i < 10; i++)
            {
                form.Location = new Point(original.X + rnd.Next(-shake_amplitude, shake_amplitude), original.Y + rnd.Next(-shake_amplitude, shake_amplitude));
                System.Threading.Thread.Sleep(20);
            }
            form.Location = original;
        }

        private void CheckEnter(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                logInMethod();
            }
        }

    }
}
