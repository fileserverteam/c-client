﻿using c_client.connection;

namespace c_client
{
    partial class MainForm
    {
        internal Client client { get; set; }
        string currentPath { get; set; }
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.responseBox = new System.Windows.Forms.GroupBox();
            this.refreshResponseButton = new System.Windows.Forms.Button();
            this.responseListView = new System.Windows.Forms.ListView();
            this.Command = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Info = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.removeElement = new System.Windows.Forms.Button();
            this.createDirecoryButton = new System.Windows.Forms.Button();
            this.dataGroupBox = new System.Windows.Forms.GroupBox();
            this.folderBox = new System.Windows.Forms.ListBox();
            this.downlowadTrackBar = new System.Windows.Forms.TrackBar();
            this.saveFileButton = new System.Windows.Forms.Button();
            this.sendFileButton = new System.Windows.Forms.Button();
            this.goButton = new System.Windows.Forms.Button();
            this.gotoDirTextBox = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.folderLabel = new System.Windows.Forms.Label();
            this.filesLabel = new System.Windows.Forms.Label();
            this.filesBox = new System.Windows.Forms.ListBox();
            this.removeFileButton = new System.Windows.Forms.Button();
            this.createDirTextBox = new System.Windows.Forms.TextBox();
            this.responseManagerBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.responseManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.responseBox.SuspendLayout();
            this.dataGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.downlowadTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.responseManagerBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.responseManagerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // responseBox
            // 
            this.responseBox.Controls.Add(this.refreshResponseButton);
            this.responseBox.Controls.Add(this.responseListView);
            this.responseBox.Controls.Add(this.removeElement);
            this.responseBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.responseBox.ForeColor = System.Drawing.Color.LightSalmon;
            this.responseBox.Location = new System.Drawing.Point(536, 12);
            this.responseBox.Name = "responseBox";
            this.responseBox.Size = new System.Drawing.Size(355, 459);
            this.responseBox.TabIndex = 1;
            this.responseBox.TabStop = false;
            this.responseBox.Text = "responseBox";
            // 
            // refreshResponseButton
            // 
            this.refreshResponseButton.ForeColor = System.Drawing.Color.LimeGreen;
            this.refreshResponseButton.Location = new System.Drawing.Point(295, 74);
            this.refreshResponseButton.Name = "refreshResponseButton";
            this.refreshResponseButton.Size = new System.Drawing.Size(30, 192);
            this.refreshResponseButton.TabIndex = 3;
            this.refreshResponseButton.Text = "RELOAD";
            this.refreshResponseButton.UseVisualStyleBackColor = true;
            this.refreshResponseButton.Click += new System.EventHandler(this.refreshResponseButton_Click);
            // 
            // responseListView
            // 
            this.responseListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Command,
            this.Info});
            this.responseListView.GridLines = true;
            this.responseListView.Location = new System.Drawing.Point(33, 74);
            this.responseListView.Name = "responseListView";
            this.responseListView.Size = new System.Drawing.Size(245, 384);
            this.responseListView.TabIndex = 2;
            this.responseListView.UseCompatibleStateImageBehavior = false;
            this.responseListView.View = System.Windows.Forms.View.Details;
            // 
            // Command
            // 
            this.Command.Text = "CMD";
            this.Command.Width = 69;
            // 
            // Info
            // 
            this.Info.Text = "Info";
            this.Info.Width = 155;
            // 
            // removeElement
            // 
            this.removeElement.ForeColor = System.Drawing.Color.Red;
            this.removeElement.Location = new System.Drawing.Point(295, 272);
            this.removeElement.Name = "removeElement";
            this.removeElement.Size = new System.Drawing.Size(30, 187);
            this.removeElement.TabIndex = 1;
            this.removeElement.Text = "REMOVE";
            this.removeElement.UseVisualStyleBackColor = true;
            this.removeElement.Click += new System.EventHandler(this.removeElement_Click);
            // 
            // createDirecoryButton
            // 
            this.createDirecoryButton.BackColor = System.Drawing.SystemColors.Info;
            this.createDirecoryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createDirecoryButton.ForeColor = System.Drawing.Color.Salmon;
            this.createDirecoryButton.Location = new System.Drawing.Point(352, 29);
            this.createDirecoryButton.Name = "createDirecoryButton";
            this.createDirecoryButton.Size = new System.Drawing.Size(134, 55);
            this.createDirecoryButton.TabIndex = 2;
            this.createDirecoryButton.Text = "createDirecory";
            this.createDirecoryButton.UseVisualStyleBackColor = false;
            this.createDirecoryButton.Click += new System.EventHandler(this.createDirecoryButton_Click);
            // 
            // dataGroupBox
            // 
            this.dataGroupBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.dataGroupBox.Controls.Add(this.createDirTextBox);
            this.dataGroupBox.Controls.Add(this.removeFileButton);
            this.dataGroupBox.Controls.Add(this.filesLabel);
            this.dataGroupBox.Controls.Add(this.filesBox);
            this.dataGroupBox.Controls.Add(this.folderLabel);
            this.dataGroupBox.Controls.Add(this.folderBox);
            this.dataGroupBox.Controls.Add(this.downlowadTrackBar);
            this.dataGroupBox.Controls.Add(this.saveFileButton);
            this.dataGroupBox.Controls.Add(this.sendFileButton);
            this.dataGroupBox.Controls.Add(this.goButton);
            this.dataGroupBox.Controls.Add(this.gotoDirTextBox);
            this.dataGroupBox.Controls.Add(this.createDirecoryButton);
            this.dataGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataGroupBox.ForeColor = System.Drawing.Color.LightSalmon;
            this.dataGroupBox.Location = new System.Drawing.Point(25, 12);
            this.dataGroupBox.Name = "dataGroupBox";
            this.dataGroupBox.Size = new System.Drawing.Size(493, 458);
            this.dataGroupBox.TabIndex = 3;
            this.dataGroupBox.TabStop = false;
            this.dataGroupBox.Text = "dataGroupBox";
            // 
            // folderBox
            // 
            this.folderBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.folderBox.FormattingEnabled = true;
            this.folderBox.IntegralHeight = false;
            this.folderBox.ItemHeight = 29;
            this.folderBox.Location = new System.Drawing.Point(13, 99);
            this.folderBox.Name = "folderBox";
            this.folderBox.Size = new System.Drawing.Size(249, 102);
            this.folderBox.TabIndex = 11;
            // 
            // downlowadTrackBar
            // 
            this.downlowadTrackBar.AccessibleName = "";
            this.downlowadTrackBar.Location = new System.Drawing.Point(351, 263);
            this.downlowadTrackBar.Minimum = 1;
            this.downlowadTrackBar.Name = "downlowadTrackBar";
            this.downlowadTrackBar.Size = new System.Drawing.Size(135, 56);
            this.downlowadTrackBar.TabIndex = 10;
            this.downlowadTrackBar.Value = 8;
            // 
            // saveFileButton
            // 
            this.saveFileButton.BackColor = System.Drawing.SystemColors.Info;
            this.saveFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.saveFileButton.ForeColor = System.Drawing.Color.Salmon;
            this.saveFileButton.Location = new System.Drawing.Point(352, 189);
            this.saveFileButton.Name = "saveFileButton";
            this.saveFileButton.Size = new System.Drawing.Size(134, 55);
            this.saveFileButton.TabIndex = 6;
            this.saveFileButton.Text = "downloadFile";
            this.saveFileButton.UseVisualStyleBackColor = false;
            this.saveFileButton.Click += new System.EventHandler(this.saveFileButton_Click);
            // 
            // sendFileButton
            // 
            this.sendFileButton.BackColor = System.Drawing.SystemColors.Info;
            this.sendFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sendFileButton.ForeColor = System.Drawing.Color.Salmon;
            this.sendFileButton.Location = new System.Drawing.Point(352, 125);
            this.sendFileButton.Name = "sendFileButton";
            this.sendFileButton.Size = new System.Drawing.Size(134, 55);
            this.sendFileButton.TabIndex = 5;
            this.sendFileButton.Text = "sendFile";
            this.sendFileButton.UseVisualStyleBackColor = false;
            this.sendFileButton.Click += new System.EventHandler(this.sendFileButton_Click);
            // 
            // goButton
            // 
            this.goButton.BackColor = System.Drawing.Color.PaleGreen;
            this.goButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.goButton.ForeColor = System.Drawing.Color.Silver;
            this.goButton.Image = global::c_client.Properties.Resources.arrow2;
            this.goButton.Location = new System.Drawing.Point(257, 39);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(25, 26);
            this.goButton.TabIndex = 4;
            this.goButton.UseVisualStyleBackColor = false;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // gotoDirTextBox
            // 
            this.gotoDirTextBox.Location = new System.Drawing.Point(6, 35);
            this.gotoDirTextBox.Name = "gotoDirTextBox";
            this.gotoDirTextBox.Size = new System.Drawing.Size(257, 36);
            this.gotoDirTextBox.TabIndex = 3;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // folderLabel
            // 
            this.folderLabel.AutoSize = true;
            this.folderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.folderLabel.ForeColor = System.Drawing.Color.DimGray;
            this.folderLabel.Location = new System.Drawing.Point(10, 79);
            this.folderLabel.Name = "folderLabel";
            this.folderLabel.Size = new System.Drawing.Size(55, 17);
            this.folderLabel.TabIndex = 12;
            this.folderLabel.Text = "folders:";
            // 
            // filesLabel
            // 
            this.filesLabel.AutoSize = true;
            this.filesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.filesLabel.ForeColor = System.Drawing.Color.DimGray;
            this.filesLabel.Location = new System.Drawing.Point(3, 227);
            this.filesLabel.Name = "filesLabel";
            this.filesLabel.Size = new System.Drawing.Size(37, 17);
            this.filesLabel.TabIndex = 14;
            this.filesLabel.Text = "files:";
            // 
            // filesBox
            // 
            this.filesBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.filesBox.FormattingEnabled = true;
            this.filesBox.IntegralHeight = false;
            this.filesBox.ItemHeight = 29;
            this.filesBox.Location = new System.Drawing.Point(6, 247);
            this.filesBox.Name = "filesBox";
            this.filesBox.Size = new System.Drawing.Size(249, 102);
            this.filesBox.TabIndex = 13;
            // 
            // removeFileButton
            // 
            this.removeFileButton.BackColor = System.Drawing.SystemColors.Info;
            this.removeFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.removeFileButton.ForeColor = System.Drawing.Color.Salmon;
            this.removeFileButton.Location = new System.Drawing.Point(353, 328);
            this.removeFileButton.Name = "removeFileButton";
            this.removeFileButton.Size = new System.Drawing.Size(134, 55);
            this.removeFileButton.TabIndex = 15;
            this.removeFileButton.Text = "removeFile";
            this.removeFileButton.UseVisualStyleBackColor = false;
            this.removeFileButton.Click += new System.EventHandler(this.removeFileButton_Click);
            // 
            // createDirTextBox
            // 
            this.createDirTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createDirTextBox.Location = new System.Drawing.Point(353, 90);
            this.createDirTextBox.Name = "createDirTextBox";
            this.createDirTextBox.Size = new System.Drawing.Size(129, 22);
            this.createDirTextBox.TabIndex = 16;
            // 
            // responseManagerBindingSource1
            // 
            this.responseManagerBindingSource1.DataSource = typeof(c_client.connection.response.ResponseManager);
            // 
            // responseManagerBindingSource
            // 
            this.responseManagerBindingSource.DataSource = typeof(c_client.connection.response.ResponseManager);
            // 
            // MainForm
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(905, 477);
            this.Controls.Add(this.dataGroupBox);
            this.Controls.Add(this.responseBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.responseBox.ResumeLayout(false);
            this.dataGroupBox.ResumeLayout(false);
            this.dataGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.downlowadTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.responseManagerBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.responseManagerBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox responseBox;
        private System.Windows.Forms.Button removeElement;
        private System.Windows.Forms.Button createDirecoryButton;
        private System.Windows.Forms.GroupBox dataGroupBox;
        private System.Windows.Forms.TextBox gotoDirTextBox;
        public System.Windows.Forms.Button goButton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ListView responseListView;
        private System.Windows.Forms.ColumnHeader Command;
        private System.Windows.Forms.ColumnHeader Info;
        private System.Windows.Forms.Button sendFileButton;
        private System.Windows.Forms.Button saveFileButton;
        private System.Windows.Forms.TrackBar downlowadTrackBar;
        private System.Windows.Forms.Button refreshResponseButton;
        private System.Windows.Forms.ListBox folderBox;
        private System.Windows.Forms.BindingSource responseManagerBindingSource1;
        private System.Windows.Forms.BindingSource responseManagerBindingSource;
        private System.Windows.Forms.Label filesLabel;
        private System.Windows.Forms.ListBox filesBox;
        private System.Windows.Forms.Label folderLabel;
        private System.Windows.Forms.Button removeFileButton;
        private System.Windows.Forms.TextBox createDirTextBox;
    }
}