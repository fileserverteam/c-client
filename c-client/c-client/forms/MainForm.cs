﻿using c_client.connection;
using c_client.connection.requests;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_client
{
    public partial class MainForm : Form
    {


        internal MainForm(Client client)
        {
            InitializeComponent();
            LogInForm LogInForm = new LogInForm(client);
            LogInForm.ShowDialog();
            this.FormClosed += new FormClosedEventHandler(Form1_FormClosed);
            this.client = client;
            this.gotoDirTextBox.TextChanged += gotoTextBox_change;
            client.startResponseManagerThread();
            currentPath = client.userName;
            gotoDirTextBox.Text = currentPath;
        }

        private void MainForm_Loaded(object sender, EventArgs e)
        {

            this.gotoDirTextBox.Text = client.userName;
        }

        private void Form1_FormClosed(Object sender, FormClosedEventArgs e)
        {
            client.logOut();
        }


        private void removeElement_Click(object sender, EventArgs e)
        {
            responseListView.Items.Clear();
        }

        private void sendFileButton_Click(object sender, EventArgs e)
        {
            String filePath = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;
                }
            }
            client.sendFile(filePath, currentPath);

        }

        private void saveFileButton_Click(object sender, EventArgs e)
        {
            String directory;
            int priority;
            String path;
            if(filesBox.SelectedItem != null) { 
                path = currentPath + "/" + filesBox.SelectedItem;
                using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
                {
                    folderBrowserDialog.RootFolder = Environment.SpecialFolder.Desktop;
                    folderBrowserDialog.Description = "Choose dowload directory";
                    folderBrowserDialog.ShowNewFolderButton = false;
                    if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                    {
                        directory = folderBrowserDialog.SelectedPath;
                        using (var writer = new StringWriter())
                        {
                            using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                            {
                                provider.GenerateCodeFromExpression(new CodePrimitiveExpression(directory), writer, null);

                            }
                        }
                        priority = this.downlowadTrackBar.Value;
                        Console.WriteLine(path);
                        Console.WriteLine(priority);
                        client.download(path, directory, priority.ToString());
                    }
                }
            }
        }

        private void createDirecoryButton_Click(object sender, EventArgs e)
        {
            if (validateDir(createDirTextBox.Text))
            {
                client.call(new MKDIRRequest(currentPath, createDirTextBox.Text));
                createDirTextBox.Text = String.Empty;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
        private void gotoTextBox_change(object sender, EventArgs e)
        {
            if (validatePath(gotoDirTextBox.Text))
            {
                gotoDirTextBox.BackColor = System.Drawing.Color.White;
            }
            else
                gotoDirTextBox.BackColor = System.Drawing.Color.PaleVioletRed;
        }

        private void goButton_Click(object sender, EventArgs e)
        {
            if (validatePath(gotoDirTextBox.Text))
            {
                    refreshView();
            }
        }

        private void refreshView()
        {
            currentPath = gotoDirTextBox.Text;
            try
            {
                client.call(new LSRequest(currentPath));
                var task = Task.Run(() => readNewLS());
                if (task.Wait(TimeSpan.FromSeconds(10)) && client.responseManager.LSResponse != null)
                {
                    lock (client.responseManager.LSResponse)
                    {
                        folderBox.Items.Clear();
                        filesBox.Items.Clear();
                        foreach (string s in client.responseManager.LSResponse.dirs)
                        {
                            folderBox.Items.Add(s);
                        }
                        foreach (string s in client.responseManager.LSResponse.files)
                        {
                            filesBox.Items.Add(s);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Refresh error");
            }
        }

        private bool validatePath(string text)
        {
            Regex r  = new Regex("^[a-zA-Z0-9/]*$");
            if (r.IsMatch(text))
                return true;
            return false;
        }

        private bool validateDir(String text)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (r.IsMatch(text))
                return true;
            return false;
        }

        void readNewLS()
        {
            while (client.responseManager.LSResponse == null) ;
        }

        private void refreshResponseButton_Click(object sender, EventArgs e)
        {
            lock (client.infoList)
            {
                foreach (InfoElement infoElement in client.infoList) 
                {
                    string[] row = {infoElement.command, infoElement.moreInfo};
                    var listViewItem = new ListViewItem(row);
                    responseListView.Items.Add(listViewItem);
                }
                client.infoList.Clear();
            }
        }

        private void removeFileButton_Click(object sender, EventArgs e)
        {
            if (filesBox.SelectedItem != null)
            {
                String path = currentPath + "/" + filesBox.SelectedItem;
                client.call(new RMRequest(path));
                refreshView();
                refreshView();
            }
        }
    }
}
