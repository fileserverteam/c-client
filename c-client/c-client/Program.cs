﻿using c_client.connection;
using c_client.connection.requests;
using c_client.LogIn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_client
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
           StartClient();
        }

        public static void StartClient()
        {
            Client client;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                client = new Client(new ConnectionInfo("168.63.56.27", 8888));
                Application.Run(new MainForm(client));
                return;
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
                MessageBox.Show("Connection error", "Server Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
