﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace c_client.connection
{
    class Connection
    {
        public Connection(string ip, int port) 
        {
            IPAddress ipAddress = System.Net.IPAddress.Parse(ip);
            remoteEP = new System.Net.IPEndPoint(ipAddress, port);
            Socket socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(remoteEP);
            Console.WriteLine("Connected");
            client = socket;
            networkStream = new NetworkStream(client);
            streamReader = new StreamReader(networkStream);
            streamWriter = new StreamWriter(networkStream);
        }

        public void close()
        {
            Thread.Sleep(2000);
            streamReader.Close();
            streamWriter.Close();
            networkStream.Close();
            client.Shutdown(SocketShutdown.Both);
            client.Close();
        }

        public Socket client { get; }
        public IPEndPoint remoteEP { get; }
        public StreamReader streamReader { get; }
        public StreamWriter streamWriter { get; }
        private NetworkStream networkStream;
    }
}
