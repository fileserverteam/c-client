﻿using c_client.connection.requests;
using c_client.connection.response;
using c_client.LogIn;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace c_client.connection
{
    class Client
    {
        private static String response = String.Empty;
        public String userName { get; set; }
        public Connection connection { get; }
        public ResponseManager responseManager { get; }
        private Thread responseManagerThread;
        List<Thread> sendingThreads = new List<Thread>();
        public ObservableCollection<InfoElement> infoList { get; set; }
        public Client(ConnectionInfo connectionInfo)
        {
            connection = new Connection(connectionInfo.ip, connectionInfo.port);
            responseManager = new ResponseManager(this);
            infoList = new ObservableCollection<InfoElement>();
        }


        public void call(Request request)
        {
            Console.WriteLine(request.getJson());
            lock (connection.streamWriter)
            {
                connection.streamWriter.Write(request.getJson());
                connection.streamWriter.Flush();
            }
        }


        //only synchronous request 
        public bool logIn(LogInRequest logInRequest)
        {
            Console.WriteLine(logInRequest.getJson());
            connection.streamWriter.Write(logInRequest.getJson());
            connection.streamWriter.Flush();
            String jsonResponse = connection.streamReader.ReadLine();
            Console.WriteLine(jsonResponse);
            GResponse response = JsonConvert.DeserializeObject<GResponse>(jsonResponse);
            if (response.code == 200)
            {
                Console.WriteLine("Connected");
                userName = logInRequest.username;
                return true;
            }
            else
                return false;
        }

        public void startResponseManagerThread()
        {
            responseManagerThread = new Thread(new ThreadStart(responseManager.run));
            responseManagerThread.Start();
        }

        public void sendFile(String path,String dest)
        {
            
            int MAX_SIZE = 1000;
            String name = Path.GetFileName(path);
            //infoList.Add(new InfoElement("DWL", name));
            new Thread(() =>
            {
                try
                {
                    using (BinaryReader binaryReader = new BinaryReader(File.Open(path,FileMode.Open)))
                    {
                        byte[] buff = new byte[MAX_SIZE];
                        int len = 0;
                        while ((len = binaryReader.Read(buff, 0, MAX_SIZE)) != 0)
                        {
                        string base64Text;
                        
                         if (len == MAX_SIZE)
                                base64Text = Convert.ToBase64String(buff);
                            else
                                base64Text = Convert.ToBase64String(buff.Take(len).ToArray());
                        call(new UPLRequest(dest, name, base64Text));
                        buff = new byte[MAX_SIZE];
                        }
                    }
                    call(new UPLFinRequest(dest, name));
                    Thread.Sleep(200);
                    //infoList.Remove(infoList.Find(x => x.file == name));
                }
                catch (Exception e)
                {
                    // Let the user know what went wrong.
                    Console.WriteLine("The file could not be read:");
                    Console.WriteLine(e.Message);
                }
            }).Start();
            
            }

        public void download(String path, String localDir,string priority)
        {
            String name = Path.GetFileName(path);
            String localPath = localDir + '\\'+name;
            responseManager.dwlDictionary.Add(path, localPath);
            call(new DWLRequest(path, priority));
        }


        public void logOut()
        {
            Console.WriteLine("Disconnecting...");
            responseManager.endEvent.Set();

            connection.close();
            Console.WriteLine("Disconnected");
        }
    }
}
