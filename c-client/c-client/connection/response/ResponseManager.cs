﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace c_client.connection.response
{
    class ResponseManager
    {
        public ResponseManager(Client client)
        {
            this.client = client;
            this.connection = client.connection;
            endEvent = new ManualResetEvent(false);
            dwlDictionary = new Dictionary<string, string>();
        }

        public void run()
        {
            String jsonResponse;
            while (true)
            {
                if (endEvent.WaitOne(0, false))
                    return;
                try
                {
                    jsonResponse = connection.streamReader.ReadLine();
                    Console.WriteLine(jsonResponse);
                    manageResponse(jsonResponse);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }

        private void manageResponse(String jsonResponse)
        {
           if ( jsonResponse.Contains("\"command\":\"RM\"")  || jsonResponse.Contains("\"command\":\"MKDIR\""))
            {
                GResponse response = JsonConvert.DeserializeObject<GResponse>(jsonResponse);
                if (response.code == 200)
                {
                    client.infoList.Add((new InfoElement(response.command, "200  " + response.data, false)));
                }
            }


            else if (jsonResponse.Contains("\"command\":\"LS\""))
            {
                if (jsonResponse.Contains("\"code\":200")) 
                {
                    LSResponse response = JsonConvert.DeserializeObject<LSResponse>(jsonResponse);
                    LSResponse = response;
                }
                else if (jsonResponse.Contains("\"code\":409"))
                {
                    GResponse response = JsonConvert.DeserializeObject<GResponse>(jsonResponse);
                    client.infoList.Add(new InfoElement("LS", "409 : "+response.data, true));
                }
            }
            else if (jsonResponse.Contains("\"command\":\"DWL\""))
            {
                DWLResponse response = JsonConvert.DeserializeObject<DWLResponse>(jsonResponse);

                if (response.code == 206)
                {
                    String localPath = dwlDictionary[response.path];
                    using (FileStream fs = new FileStream(localPath, FileMode.Append, FileAccess.Write))
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        byte[] byteArr = Convert.FromBase64String(response.data);
                        //Console.Write(Encoding.Default.GetString(byteArr));
                        Console.WriteLine("\nPosition="+bw.BaseStream.Position.ToString());
                        bw.Write(byteArr);
                        bw.Flush();
                    }

                }
                else if (response.code == 200)
                {
                    dwlDictionary.Remove(response.path);
                    client.infoList.Add(new InfoElement("DWL", "File from: " + response.path+" finished", false));
                }
            }
            else if (jsonResponse.Contains("\"command\":\"DWLABORT\""))
            {
                DWLAbortResponse response = JsonConvert.DeserializeObject<DWLAbortResponse>(jsonResponse);
                if (response.code == 200)
                {
                    dwlDictionary.Remove(response.path);
                    client.infoList.Add(new InfoElement("DWLABORT", "200 - for file: " + response.path, false));
                }
                else if (response.code == 409)
                {
                    client.infoList.Add(new InfoElement("DWLABORT", "409 - for file: "+response.path,true));
                }
            }
        }
        Client client;
        private Connection connection;
        public LSResponse LSResponse { get; set; }
        public ManualResetEvent endEvent { get; set; }
        public Dictionary<string, string> dwlDictionary;
    }
}
