﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.response
{
    class DWLAbortResponse :Response
    {
        public DWLAbortResponse(string type, string command, int code, string path)
        {
            this.type = type;
            this.command = command;
            this.code = code;
            this.path = path;
        }

        public string type { get; set; }
        public string command { get; set; }
        public int code { get; set; }
        public string path { get; set; }
    }
}
