﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.response
{
    class LSResponse :Response
    {
        public string type { get; set; }
        public int code { get; set; }
        public string command { get; set; }
        public string path { get; set; }
        public List<string> files { get; set; }
        public List<string> dirs { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
