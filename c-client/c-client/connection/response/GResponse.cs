﻿using c_client.connection.response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.response
{
    class GResponse :Response 
    {
        public GResponse(int code, string data, string command, string type)
        {
            this.code = code;
            this.data = data;
            this.command = command;
            this.type = type;
        }

        public int code { get; set; }
        public string data { get; set; }
        public string command { get; set; }
        public string type { get; set; }

       
        public override string ToString()
        {
            return "code: "+code + "\ndata: " + data+ "\ntype: " + type+"\ncommand:"+command;
        }
    }
}
