﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.response
{
    class DWLResponse : Response
    {
        public DWLResponse(string type, string command, int code, string path, string data)
        {
            this.type = type;
            this.command = command;
            this.code = code;
            this.path = path;
            this.data = data;
        }

        
        public string path { get;  }
        public string data { get;  }
        public string type { get;  }
        public string command { get; }
        public int code { get;  }
    }
}
