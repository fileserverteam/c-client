﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection
{
    class ConnectionInfo
    {
        public ConnectionInfo(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        public string ip { get; }
        public int port { get; }
    }
}
