﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection
{
    class InfoElement
    {

        public InfoElement(string command, string moreInfo, bool isError)
        {
            this.command = command;
            this.moreInfo = moreInfo;
            this.isError = isError;
        }

        public String command { get; set; }
        public String moreInfo { get; set; }
        public bool isError { get; set; }
    }
}
