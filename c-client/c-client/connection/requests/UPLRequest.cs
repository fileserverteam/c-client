﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.requests
{
    class UPLRequest : Request
    {
        public UPLRequest(string path, string name, string data)
        {
            type = "REQUEST";
            command = "UPL";
            this.name = name;
            this.path = path;
            this.data = data;
        }

        public string type { get; }
        public string command { get; }
        public string name { get; }
        public string path { get; }
        public string data { get; }

        public String getJson()
        {
            return (JsonConvert.SerializeObject(this) + "\0");
        }
    }
}
