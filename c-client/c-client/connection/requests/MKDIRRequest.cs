﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.requests
{
    class MKDIRRequest : Request
    {
        public MKDIRRequest( string path, string name)
        {
            type = "REQUEST";
            command = "MKDIR";
            this.name = name;
            this.path = path;
        }

        public string type { get; set; }
        public string command { get; set; }
        public string name { get; set; }
        public string path { get; set; }

        public String getJson()
        {
            return (JsonConvert.SerializeObject(this) + "\0");
        }
    }
}
