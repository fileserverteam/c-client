﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.requests
{
    class UPLFinRequest : Request
    {
        public UPLFinRequest(string path, string name)
        {
            type = "REQUEST";
            command = "UPLFIN";
            this.name = name;
            this.path = path;
        }

        public string type { get; }
        public string command { get; }
        public string name { get; }
        public string path { get; }

        public String getJson()
        {
            return (JsonConvert.SerializeObject(this) + "\0");
        }
    }
}
