﻿using c_client.connection.requests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace c_client.LogIn
{
    class LogInRequest : Request
    {
        public string type { get; }
        public  string command { get; }
        public string username { get; set; }
        public string password { get; set; }

        public LogInRequest(string username, string password)
        {
            type = "REQUEST";
            command = "AUTH";
            this.username = username;
            this.password = password;
        }

        public String getJson()
        {
            return (JsonConvert.SerializeObject(this) + "\0");
        }
  
    }
}
