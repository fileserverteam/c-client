﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.requests
{
    class DWLRequest : Request
    {
        public DWLRequest(string path, string priority)
        {
            type = "REQUEST";
            command = "DWL";
            this.path = path;
            this.priority = priority;
        }

        public string type { get;  }
        public string command { get;  }
        public string path { get;  }
        public string priority { get;  }

        public String getJson()
        {
            return (JsonConvert.SerializeObject(this) + "\0");
        }
    }
}
