﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.requests
{
    class RMRequest : Request
    {
        public RMRequest(string path)
        {
            type = "REQUEST";
            command = "RM";
            this.path = path;
        }

        public string type { get; set; }
        public string command { get; set; }
        public string path { get; set; }

        public String getJson()
        {
            return (JsonConvert.SerializeObject(this) + "\0");
        }
    }
}
