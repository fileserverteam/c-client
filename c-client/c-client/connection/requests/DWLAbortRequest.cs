﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.requests
{
    class DWLAbortRequest : Request
    {
        public DWLAbortRequest(string path)
        {
            type = "REQUEST";
            command = "DWLABORT";
            this.path = path;
        }

        public string type { get; }
        public string command { get; }
        public string path { get; }

        public String getJson()
        {
            return (JsonConvert.SerializeObject(this) + "\0");
        }
    }
}
