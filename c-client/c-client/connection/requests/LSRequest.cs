﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.requests
{
    class LSRequest : Request
    {
        public string type { get; }
        public string command { get; }
        public string path { get; }

        public LSRequest(string path)
        {
            type = "REQUEST";
            command = "LS";
            this.path = path;
        }

        public String getJson()
        {
            return (JsonConvert.SerializeObject(this) + "\0");
        }
    }

}
