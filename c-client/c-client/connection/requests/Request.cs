﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_client.connection.requests
{
    interface Request
    {
        String getJson();
        string type { get; }
        string command { get; }
    }
}
